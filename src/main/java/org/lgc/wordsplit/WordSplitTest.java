package org.lgc.wordsplit;

import java.util.List;

import org.apdplat.word.WordSegmenter;
import org.apdplat.word.segmentation.SegmentationAlgorithm;
import org.apdplat.word.segmentation.Word;
import org.apdplat.word.segmentation.WordRefiner;
import org.apdplat.word.tagging.SynonymTagging;

/**
/* 版权所有： 广州敏道科技有限公司
/*
/* 功能描述：
/*
/* 创 建 人：李国才
/* 创建时间：2016年5月20日 下午8:49:43 	
 **/
public class WordSplitTest {
	public static void main(String[] args) {
		 
        for(SegmentationAlgorithm segmentationAlgorithm : SegmentationAlgorithm.values()){
        	
            System.out.println("开始评估 word分词 "+segmentationAlgorithm.name()+":"+segmentationAlgorithm.getDes());

        }
//		//移除停用词：
		List<Word> words2 = WordSegmenter.seg("杨尚川是APDPlat应用级产品开发平台的作者");
//		List<Word> words = WordSegmenter.segWithStopWords("杨尚川是APDPlat应用级产品开发平台的作者");
//		            System.out.println(words);
		System.out.println(words2);
		
		

		Long startT=System.currentTimeMillis();
		//List<Word> words = WordSegmenter.segWithStopWords("楚离陌千方百计为无情找回记忆");
		//List<Word> words = WordSegmenter.seg("楚离陌千方百计为无情找回记忆");
		String[] nowProblems={"您好，请问您哪里不舒服啊","您这个症状什么时候开始的","您是在什么情况下出现这个症状的","你是从什么时候开始这样的","除了这个症状，还有其他地方疼吗","欢迎使用ansj_seg,(ansj中文分词)在这里如果你遇到什么问题都可以联系我.我一定尽我所能.帮助大家.ansj_seg更快,更准,更自由!"};
		for (String nowProblem: nowProblems){
			System.out.println("=================================================");
			List<Word> words = WordSegmenter.segWithStopWords(nowProblem,SegmentationAlgorithm.FullSegmentation);
			System.out.println("原始分词结果："+words);
			//List<Word> words = WordSegmenter.seg(nowProblem,SegmentationAlgorithm.BidirectionalMaximumMinimumMatching); 
			//List<Word> words = WordSegmenter.seg(nowProblem,SegmentationAlgorithm.MinimalWordCount); 
			List<Word> words3 =WordSegmenter.seg(nowProblem);
			//System.out.println(words);
			words = WordRefiner.refine(words3);
			//SynonymTagging.process(words, false);
			System.out.println("去掉停用词和重新定义后的结果："+words);
			
//			SynonymTagging.process(words,true);
//			System.out.println(words);
		}
		Long endT=System.currentTimeMillis();
		System.out.println("用时："+(endT-startT));
//		结果如下：
//		[楚离陌, 千方百计, 为, 无情, 找回, 记忆]
//		做同义标注：
//		SynonymTagging.process(words,false);
//		System.out.println(words);
//		结果如下：
//		[楚离陌, 千方百计[久有存心, 化尽心血, 想方设法, 费尽心机], 为, 无情, 找回, 记忆[影象]]
//		如果启用间接同义词：
//		SynonymTagging.process(words, false);
//		System.out.println(words);
//		结果如下：
//		[楚离陌, 千方百计[久有存心, 化尽心血, 想方设法, 费尽心机], 为, 无情, 找回, 记忆[影像, 影象]]
//
//		List<Word> words = WordSegmenter.segWithStopWords("手劲大的老人往往更长寿");
//		System.out.println(words);
//		结果如下：
//		[手劲, 大, 的, 老人, 往往, 更, 长寿]
//		做同义标注：
//		SynonymTagging.process(words);
//		System.out.println(words);
//		结果如下：
//		[手劲, 大, 的, 老人[白叟], 往往[常常, 每每, 经常], 更, 长寿[长命, 龟龄]]
//		如果启用间接同义词：
//		SynonymTagging.process(words, false);
//		System.out.println(words);
//		结果如下：
//		[手劲, 大, 的, 老人[白叟], 往往[一样平常, 一般, 凡是, 寻常, 常常, 常日, 平凡, 平居, 平常, 平日, 平时, 往常, 日常, 日常平凡, 时常, 普通, 每每, 泛泛, 素日, 经常, 通俗, 通常], 更, 长寿[长命, 龟龄]]
//
//		以词“千方百计”为例：
//		可以通过Word的getSynonym()方法获取同义词如：
//		System.out.println(word.getSynonym());
//		结果如下：
//		[久有存心, 化尽心血, 想方设法, 费尽心机]
//		注意：如果没有同义词，则getSynonym()返回空集合：Collections.emptyList()
//
//		间接同义词和直接同义词的区别如下：
//		假设：
//		A和B是同义词，A和C是同义词，B和D是同义词，C和E是同义词
//		则：
//		对于A来说，A B C是直接同义词
//		对于B来说，A B D是直接同义词
//		对于C来说，A C E是直接同义词
//		对于A B C来说，A B C D E是间接同义词
	}

}
